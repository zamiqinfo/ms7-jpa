package az.ingress.ms7jpa.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "students")
@EqualsAndHashCode(exclude = "group")
@ToString(exclude = "classes")
@Data
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String email;

    private int age;

    @OneToOne(cascade = CascadeType.PERSIST)
    private StudentDetails studentDetails;

    @ManyToOne()
    private Group group;

    @OneToMany(cascade = CascadeType.ALL)
    private Set<Classes> classes;
}
