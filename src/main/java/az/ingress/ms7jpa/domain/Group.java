package az.ingress.ms7jpa.domain;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
@ToString(exclude = "students")
@Table(name = "s_group")
public class Group {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String name;

    @OneToMany(mappedBy = "group",cascade = CascadeType.PERSIST,fetch = FetchType.LAZY)
    Set<Student> students;
}
