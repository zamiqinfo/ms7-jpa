package az.ingress.ms7jpa.domain;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class StudentDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    private String about;

}
