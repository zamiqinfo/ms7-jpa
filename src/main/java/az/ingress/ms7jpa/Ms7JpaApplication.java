package az.ingress.ms7jpa;

import az.ingress.ms7jpa.domain.*;
import az.ingress.ms7jpa.repository.GroupRepository;
import az.ingress.ms7jpa.repository.StudentRepository;
import com.speedment.jpastreamer.application.JPAStreamer;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.Transactional;
import java.util.Collections;
import java.util.Set;

@SpringBootApplication
@RequiredArgsConstructor
public class Ms7JpaApplication implements CommandLineRunner {

	private final GroupRepository groupRepository;
	private final StudentRepository studentRepository;
	private final EntityManagerFactory entityManagerFactory;
	public static void main(String[] args) {
		SpringApplication.run(Ms7JpaApplication.class, args);
	}

	@Override
//	@Transactional
	public void run(String... args) throws Exception {
//		getStudentByJpaStreamer();
//		insertDataToDB();
//		getGroupsFromDB();
//		getGroupsByJPQL();
		insertDataClasses();

//		StudentDetails studentDetails=new StudentDetails();
//		studentDetails.setAbout("Test");
//		student.setStudentDetails(studentDetails);
//		studentRepository.save(student);

	}


	private void insertDataClasses() {
		Group group=new Group();
		group.setName("Group");

		Classes classesA=new Classes();
		classesA.setName("A");

		Classes classesB=new Classes();
		classesB.setName("B");

		Student student1 = new Student();
		student1.setName("Zamiq");
		student1.setEmail("eliyev.zamiq@gmail.com");
		student1.setAge(35);
		student1.setClasses(Set.of(classesA));
		classesA.setStudent(student1);
		student1.setGroup(group);

		Student student2 = new Student();
		student2.setName("Cavid");
		student2.setEmail("eliyev.cavid@gmail.com");
		student2.setAge(30);
		student2.setClasses(Set.of(classesB));
		classesB.setStudent(student2);
		student2.setGroup(group);

		group.setStudents(Set.of(student1,student2));

		groupRepository.save(group);

		System.out.println("id is" + group.getId());

		EntityManager entityManager = entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();

		EntityGraph<?> entityGraph = entityManager.createEntityGraph("graph.GroupClasses");

		group = entityManager.find(Group.class, group.getId(), Collections.singletonMap("javax.persistence.loadgraph", entityGraph));
		System.out.println(group);

		entityManager.close();
	}

	private void getGroupsByJPQL() {
		groupRepository.getAllGroupsByQuerry().stream().forEach(System.out::println);
	}

	private void getGroupsFromDB() {
		groupRepository.findAll().stream().forEach(System.out::println);
	}

	private void insertDataToDB() {
		for (int i=0;i<100;i++) {
			Student student = new Student();
			student.setName("Zamiq"+i);
			student.setEmail("eliyev.zamiq@gmail.com");
			student.setAge(35);

			Student student2 = new Student();
			student2.setName("Cavid"+i);
			student2.setEmail("eliyev.cavid@gmail.com");
			student2.setAge(30);

			Group group = new Group();

			student.setGroup(group);
			student2.setGroup(group);

			group.setName("Class N-"+i);
			group.setStudents(Set.of(student, student2));

			groupRepository.save(group);
		}
	}

	private void getStudentByJpaStreamer(){
		JPAStreamer jpaStreamer=JPAStreamer.of(entityManagerFactory);

		jpaStreamer.stream(Student.class)
				.filter(Student$.name.startsWith("Z"))
				.filter(Student$.age.greaterThan(25))
				.skip(10)
				.limit(5)
				.forEach(System.out::println);
	}
	private void createStudents(){
		for (int i=0;i<10;i++){
			Student student=new Student();
			student.setName("Zamiq"+i);
			student.setEmail("eliyev.zamiq@gmail.com");
			student.setAge(25+i);
			studentRepository.save(student);
		}
		//		studentRepository.findAll().stream().forEach(System.out::println);
//		studentRepository.findByAgeQuery(30).stream().forEach(System.out::println);
	}
}
