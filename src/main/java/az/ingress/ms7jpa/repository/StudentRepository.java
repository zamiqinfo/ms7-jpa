package az.ingress.ms7jpa.repository;

import az.ingress.ms7jpa.domain.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface StudentRepository extends JpaRepository<Student,Long> {

    @Query("select s from Student s where s.age> ?1")
    List<Student> findByAgeQuery(int age);
}
