package az.ingress.ms7jpa.repository;

import az.ingress.ms7jpa.domain.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Set;

public interface GroupRepository extends JpaRepository<Group,Long> {

    @Query("select g from Group g Join fetch g.students s")
    public Set<Group> getAllGroupsByQuerry();
}
